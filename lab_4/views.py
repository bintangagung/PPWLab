from django.shortcuts import render

def index_page(request):
	html = 'index.html'
	return render(request, html)

def education_page(request):
	html = 'education.html'
	return render(request, html)

def experience_page(request):
	html = 'experience.html'
	return render(request, html)

def skill_page(request):
	html = 'skill.html'
	return render(request, html)

def contact_page(request):
	html = 'contact.html'
	return render(request, html)

def challenge_page(request):
	html = 'challenge.html'
	return render(request, html)