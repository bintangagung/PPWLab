from django.conf.urls import url
from .views import index_page, education_page, experience_page, skill_page, contact_page, challenge_page
urlpatterns = [
    url(r'^$', index_page, name='index'),
    url(r'^education$', education_page, name='education'),
    url(r'^experience$', experience_page, name='experience'),
    url(r'^skill$', skill_page, name='skill'),
    url(r'^contact$', contact_page, name='contact'),
    url(r'^challenge$', challenge_page, name='challenge')
]
