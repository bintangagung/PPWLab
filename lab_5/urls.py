from django.conf.urls import url
from lab_5 import views
from .views import FormKegiatan_page, JadwalKegiatan_page 
urlpatterns = [
    url(r'^$', views.FormKegiatan_page, name='FormKegiatan'),
    url(r'^JadwalKegiatan$', JadwalKegiatan_page, name='JadwalKegiatan')
]