from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import reverse
from .forms import EventForm
from .models import JadwalPribadi
# Create your views here.
def FormKegiatan_page (request):
	# html = 'FormKegiatan.html'
	return render(request, "FormKegiatan.html")

def JadwalKegiatan_page (request):
	html = 'JadwalKegiatan.html'
	return render(request, html)


def personal_schedule(request):
	events = EventForm.objects.all().values()

	if request.method == "POST":
		form = EventForm(request.POST)
		if form.is_valid():
			form.save()
			return HttpResponseRedirect(reverse('schedule'))
	else:
		form = EventForm()

	response = {'form': form, 'events': events}
	return render(request, 'JadwalKegiatan.html', {'form': form, 'events': events})
