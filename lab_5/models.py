from django.db import models
from django.utils import timezone
from datetime import datetime, date
# Create your models here.

class JadwalPribadi(models.Model):
	event = models.CharField(max_length=30)
	category = models.CharField(max_length=20)
	date = models.DateTimeField()
	location = models.CharField(max_length=20)
	content = models.CharField(max_length=120)
	published_date = models.DateTimeField(default=timezone.now)
