from django import forms
from .models import JadwalPribadi

class EventForm(forms.ModelForm):
	class Meta:
		model = JadwalPribadi
		fields = ('event', 'category', 'date', 'location', 'content', 'published_date')


